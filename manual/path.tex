\section{Module \module{path}}

\sectionauthor{J\"org Lehmann}{joergl@users.sourceforge.net} 

\label{path}

The \module{path} module defines several important classes which are
documented in the present section.

\subsection{Class \class{path} --- PostScript-like paths}

\label{path:path}

\declaremodule{}{path}

\begin{classdesc}{path}{*pathitems}
This class represents a PostScript like path consisting of the
path elements \var{pathitems}.

All possible path items are described in Sect.~\ref{path:pathitem}.
Note that there are restrictions on the first path element and likewise
on each path element after a \class{closepath} directive. In both cases,
no current point is defined and the path element has to be an instance
of one of the following classes: \class{moveto}, \class{arc}, and
\class{arcn}.
\end{classdesc}

Instances of the class \class{path} provide the following
methods (in alphabetic order):

\begin{methoddesc}{append}{pathitem}
Appends a \var{pathitem} to the end of the path.
\end{methoddesc}

\begin{methoddesc}{arclen}{}
Returns the total arc length of the path.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{arclentoparam}{lengths}
  Returns the parameter value(s) corresponding to the arc length(s)
  \var{lengths}.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{at}{params}
  Returns the coordinates (as 2-tuple) of the path point(s)
  corresponding to the parameter value(s) \var{params}.$^\ddagger$
  $^\dagger$
\end{methoddesc}

\begin{methoddesc}{atbegin}{}
  Returns the coordinates (as 2-tuple) of the first point of the path.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{atend}{}
  Returns the coordinates (as 2-tuple) of the end point of the path.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{bbox}{}
  Returns the bounding box of the path. Note that this returned
  bounding box may be too large, if the path contains any
  \class{curveto} elements, since for these the control box, i.e., the
  bounding box enclosing the control points of the B\'ezier curve is
  returned. 
\end{methoddesc}

\begin{methoddesc}{begin}{}
  Returns the parameter value (a \class{normpathparam} instance) of
  the first point in the path. 
\end{methoddesc}

\begin{methoddesc}{curveradius}{param=None, arclen=None}
  Returns the curvature radius/radii (or None if infinite) at parameter
  value(s) params.$^\ddagger$ This is the inverse of the curvature at
  this parameter. Note that this radius can be negative or positive,
  depending on the sign of the curvature.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{end}{}
  Returns the parameter value (a \class{normpathparam} instance) of
  the last point in the path. 
\end{methoddesc}

\begin{methoddesc}{extend}{pathitems}
Appends the list \var{pathitems} to the end of the path. 
\end{methoddesc}

\begin{methoddesc}{intersect}{opath}
  Returns a tuple consisting of two lists of parameter values
  corresponding to the intersection points of the path with the other
  path \var{opath}, respectively.$^\dagger$ For intersection points which
  are not farther apart then \var{epsilon} points, only one is returned. 
\end{methoddesc}

\begin{methoddesc}{joined}{opath}
  Appends \var{opath} to the end of the path, thereby merging the last
  subpath (which must not be closed) of the path with the first sub
  path of \var{opath} and returns the resulting new path.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{normpath}{epsilon=None}
  Returns the equivalent \class{normpath}. For the conversion and for later
  calculations with this \class{normpath} and accuracy of
  \var{epsilon} points is used. If \var{epsilon} is \var{None}, the
  global \var{epsilon} of the \module{path} module is used. 
\end{methoddesc}

\begin{methoddesc}{paramtoarclen}{params}
  Returns the arc length(s) corresponding to the parameter value(s)
  \var{params}.$^\ddagger$ $^\dagger$
\end{methoddesc}


\begin{methoddesc}{range}{}
  Returns the maximal parameter value \var{param} that is allowed in the
  path methods. 
\end{methoddesc}

\begin{methoddesc}{reversed}{}
  Returns the reversed path.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{rotation}{params}
  Returns (a) rotations(s) which (each), which rotate the x-direction
  to the tangent and the y-direction to the normal at that param.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{split}{params}
  Splits the path at the parameter values \var{params}, which have to
  be sorted in ascending order, and returns a corresponding list of
  \class{normpath} instances.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{tangent}{params, length=1}
  Return (a) \class{line} instance(s) corresponding to the tangent
  vector(s) to the path at the parameter value(s)
  \var{params}.$^\ddagger$ The tangent vector will be scaled to the length
  \var{length}.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{trafo}{params}
  Returns (a) trafo(s) which (each) translate to a point on the path
  corresponding to the param, rotate the x-direction to the tangent
  and the y-direction to the normal in that point.$^\dagger$
\end{methoddesc}

\begin{methoddesc}{transformed}{trafo}
  Returns the path transformed according to the linear transformation
  \var{trafo}. Here, \texttt{trafo} must be an instance of the
  \class{trafo.trafo} class.$^\dagger$
\end{methoddesc}

Some notes on the above:
\begin{itemize}
\item The $\dagger$ denotes methods which require a prior
  conversion of the path into a \class{normpath} instance. This is
  done automatically (using the precision \var{epsilon} set globally
  using \method{path.set}). If you need a different \var{epsilon} for
  a normpath, you also can perform the conversion manually. 
\item Instead of using the \method{joined()} method, you can also join two
paths together with help of the \verb|<<| operator, for instance
\samp{p = p1 << p2}. 
\item $^\ddagger$ In these methods, \var{params} may either be a
  single value or a list. In the latter case, the result of the method
  will be a list consisting of the results for every parameter.  The
  parameter itself may either be a length (or a number which is then
  interpreted as a user length) or an instance of the class
  \class{normpathparam}. In the former case, the length refers to the
  arc length along the path. 
\end{itemize}

\subsection{Path elements}

\label{path:pathitem}

The class \class{pathitem} is the superclass of all PostScript path
construction primitives. It is never used directly, but only by
instantiating its subclasses, which correspond one by one to the
PostScript primitives. 

Except for the path elements ending in \code{_pt}, all coordinates
passed to the path elements can be given as number (in which case they
are interpreted as user units with the currently set default type) or in
\PyX\ lengths. 

The following operation move the current point and open a new subpath:

\begin{classdesc}{moveto}{x, y}
Path element which sets the current point to the absolute coordinates (\var{x},
\var{y}). This operation opens a new subpath. 
\end{classdesc}

\begin{classdesc}{rmoveto}{dx, dy}
Path element which moves the current point by (\var{dx}, \var{dy}). 
This operation opens a new subpath. 
\end{classdesc}

Drawing a straight line can be accomplished using:

\begin{classdesc}{lineto}{x, y}
Path element which appends a straight line from the current point to the
point with absolute coordinates (\var{x}, \var{y}), which becomes
the new current point. 
\end{classdesc}

\begin{classdesc}{rlineto}{dx, dy}
Path element which appends a straight line from the current point to the
a point with relative coordinates (\var{dx}, \var{dy}), which becomes
the new current point. 
\end{classdesc}

For the construction of arc segments, the following three operations
are available:

\begin{classdesc}{arc}{x, y, r, angle1, angle2}
Path element which appends an arc segment in counterclockwise direction
with absolute coordinates (\var{x}, \var{y}) of the center and 
radius \var{r} from \var{angle1} to \var{angle2} (in degrees). 
If before the operation, the current point is defined, a straight line
is from the current point to the beginning of the arc segment is
prepended. Otherwise, a subpath, which thus is the first one in the
path, is opened. After the operation, the current point is at the end
of the arc segment. 
\end{classdesc}

\begin{classdesc}{arcn}{x, y, r, angle1, angle2}
Path element which appends an arc segment in clockwise direction
with absolute coordinates (\var{x}, \var{y}) of the center and 
radius \var{r} from \var{angle1} to \var{angle2} (in degrees). 
If before the operation, the current point is defined, a straight line
is from the current point to the beginning of the arc segment is
prepended. Otherwise, a subpath, which thus is the first one in the
path, is opened. After the operation, the current point is at the end
of the arc segment. 
\end{classdesc}

\begin{classdesc}{arct}{x1, y1, x2, y2, r}
Path element which appends an arc segment of radius \var{r}
connecting between (\var{x1}, \var{y1}) and (\var{x2}, \var{y2}).\\
\end{classdesc}

B\'ezier curves can be constructed using: \

\begin{classdesc}{curveto}{x1, y1, x2, y2, x3, y3}
Path element which appends a B\'ezier curve with
the current point as first control point and the other control points
(\var{x1}, \var{y1}), (\var{x2}, \var{y2}), and (\var{x3}, \var{y3}). 
\end{classdesc}

\begin{classdesc}{rcurveto}{dx1, dy1, dx2, dy2, dx3, dy3}
Path element which appends a B\'ezier curve with
the current point as first control point and the other control points
defined relative to the current point by the coordinates
(\var{dx1}, \var{dy1}), (\var{dx2}, \var{dy2}), and (\var{dx3}, \var{dy3}). 
\end{classdesc}

Note that when calculating the bounding box (see Sect.~\ref{bbox}) of
B\'ezier curves, \PyX{} uses for performance reasons the so-called
control box, i.e., the smallest rectangle enclosing the four control
points of the B\'ezier curve. In general, this is not the smallest
rectangle enclosing the B\'ezier curve. 

Finally, an open subpath can be closed using:

\begin{classdesc}{closepath}{}
Path element which closes the current subpath. 
\end{classdesc}

For performance reasons, two non-PostScript path elements are defined, 
which perform multiple identical operations:

\begin{classdesc}{multilineto_pt}{points_pt}
Path element which appends straight line segments starting from
the current point and going through the list of points given 
in the \var{points_pt} argument. All coordinates have to 
be given in PostScript points. 
\end{classdesc}

\begin{classdesc}{multicurveto_pt}{points_pt}
Path element which appends B\'ezier curve segments starting from
the current point and going through the list of each three control
points given in the \var{points_pt} argument. 
\end{classdesc}


\subsection{Class \class{normpath}}

\label{path:normpath}

The \class{normpath} class is used internally for all non-trivial path
operations, i.e. the ones marked by a $\dagger$ in the description of
the \class{path} above. It represents a path as a list of subpaths, which are
instances of the class \class{normsubpath}. These \class{normsubpath}s
themselves consist of a list of \class{normsubpathitems} which are
either straight lines (\class{normline}) or B\'ezier curves
(\class{normcurve}). 

A given path can easily be converted to the corresponding \class{normpath}
using the method with this name:
\begin{verbatim}
np = p.normpath()
\end{verbatim}
Additionally, you can specify the accuracy (in points) which is used
in all \class{normpath} calculations by means of the argument
\var{epsilon}, which defaults to to $10^{-5}$ points. This default
value can be changed using the module function \function{path.set}. 

To construct a \class{normpath} from a list of \class{normsubpath}
instances, you pass them to the \class{normpath} constructor:

\begin{classdesc}{normpath}{normsubpaths=[]}
  Construct a \class{normpath} consisting of \var{subnormpaths}, which
  is a list of \class{subnormpath} instances. 
\end{classdesc}

Instances of \class{normpath} offers all methods of regular
\class{path}s, which also have the same semantics. An exception are
the methods \method{append} and \method{extend}. While they allow for
adding of instances of \class{subnormpath} to the \class{normpath}
instance, they also keep the functionality of a regular path and allow
for regular path elements to be appended. The later are converted to
the proper normpath representation during addition. 

In addition to the \class{path} methods, a \class{normpath} instance
also offers the following methods, which operate on the instance
itself, i.e., modify it in place. 

\begin{methoddesc}{join}{other}
  Join \var{other}, which has to be a \class{path} instance, to
  the \class{normpath} instance. 
\end{methoddesc}

\begin{methoddesc}{reverse}{}
  Reverses the \class{normpath} instance. 
\end{methoddesc}

\begin{methoddesc}{transform}{trafo}
  Transforms the \class{normpath} instance according to the linear
  transformation \var{trafo}. 
\end{methoddesc}

Finally, we remark that the sum of a \class{normpath} and a \class{path}
always yields a \class{normpath}. 


\subsection{Class \class{normsubpath}}

\begin{classdesc}{normsubpath}{normsubpathitems=[], closed=0,
    epsilon=1e-5}
  Construct a \class{normsubpath} consisting of
  \var{normsubpathitems}, which is a list of \class{normsubpathitem}
  instances. If \var{closed} is set, the \class{normsubpath} will be
  closed, thereby appending a straight line segment from the first to
  the last point, if it is not already present. All calculations with
  the \class{normsubpath} are performed with an accuracy of \var{epsilon}. 
\end{classdesc}

Most \class{normsubpath} methods behave like the ones of a
\class{path}. 

Exceptions are:

\begin{methoddesc}{append}{anormsubpathitem}
  Append the \var{anormsubpathitem} to the end of the
  \class{normsubpath} instance. This is only possible if the
  \class{normsubpath} is not closed, otherwise an exception is raised. 
\end{methoddesc}

\begin{methoddesc}{extend}{normsubpathitems}
  Extend the \class{normsubpath} instances by \var{normsubpathitems},
  which has to be a list of \class{normsubpathitem} instances. This is
  only possible if the \class{normsubpath} is not closed, otherwise an
  exception is raised. 
\end{methoddesc}

\begin{methoddesc}{close}{}
  Close the \class{normsubpath} instance, thereby appending a straight
  line segment from the first to the last point, if it is not already
  present. 
\end{methoddesc}

\subsection{Predefined paths}

\label{path:predefined}

For convenience, some oft-used paths are already predefined. All
of them are subclasses of the \class{path} class. 

\begin{classdesc}{line}{x0, y0, x1, y1}
A straight line from the point (\var{x0}, \var{y0}) to the point (\var{x1}, \var{y1}). 
\end{classdesc}

\begin{classdesc}{curve}{x0, y0, x1, y1, x2, y2, x3, y3}
A B\'ezier curve with 
control points  (\var{x0}, \var{y0}), $\dots$, (\var{x3}, \var{y3}).\
\end{classdesc}

\begin{classdesc}{rect}{x, y, w, h}
A closed rectangle with lower left point (\var{x}, \var{y}), width \var{w}, and
  height \var{h}. 
\end{classdesc}

\begin{classdesc}{circle}{x, y, r}
A closed circle with center (\var{x}, \var{y}) and radius \var{r}. 
\end{classdesc}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "manual.tex"
%%% ispell-dictionary: "british"
%%% End:
